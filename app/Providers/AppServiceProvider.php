<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		\URL::forceRootUrl(\Config::get('app.url'));    
// And this if you wanna handle https URL scheme
// It's not usefull for http://www.example.com, it's just to make it more independant from the constant value


        if(\DB::connection()->getDatabaseName())
        {
            if( isset($config['enable_https'] ) && $config['enable_https'] != 0)
                \URL::forceSchema('https');


            Validator::extend(
                  'recaptcha',
                  'Lib\\Validators\\ReCaptcha@validate'
           );
        }
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
